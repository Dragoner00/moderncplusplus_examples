#include "FibonacciList.h"
#include <crtdbg.h>
#include <algorithm>

#include <iostream>

FibonacciList::FibonacciList(size_t size)
	: m_size(size),
	  m_numbers(0 != m_size ? new int[m_size] : nullptr)
{
	std::cout << "Constructor" << std::endl;
	if (m_size > 0) {
		m_numbers[0] = 0;
	}
	if (m_size > 1) {
		m_numbers[1] = 1;
	}

	// Dummy data: list of increasing integers
	for (size_t i = 2; i < m_size; ++i) {
		m_numbers[i] = m_numbers[i - 1] + m_numbers[i - 2];
	}
}

FibonacciList::~FibonacciList()
{
	delete m_numbers;
	m_numbers = nullptr;
}

FibonacciList::FibonacciList(const FibonacciList& rhs) noexcept
	: m_size(rhs.m_size),
	m_numbers(m_size ? new int[m_size] : nullptr)
{
	std::cout << "CopyConstructor" << std::endl;
	std::copy(rhs.m_numbers, rhs.m_numbers + m_size, m_numbers);
}

FibonacciList& FibonacciList::operator=(const FibonacciList& rhs) noexcept
{
	std::cout << "CopyAssignment" << std::endl;
	// Better use copy-and-swap? : https://stackoverflow.com/q/3279543
	if (this != &rhs) {
		delete m_numbers;
		m_numbers = nullptr;

		// Allocate new memory
		m_size = rhs.m_size;
		m_numbers = new int[m_size];

		std::copy(rhs.m_numbers, rhs.m_numbers + m_size, m_numbers);
	}
	return *this;
}

FibonacciList& FibonacciList::operator=(FibonacciList&& rhs) noexcept
{
	std::cout << "MoveAssignment" << std::endl;
	// Self-assingment in a move operator is a bug,
	// see: https://stackoverflow.com/a/9322542
	_ASSERT(this != &rhs);

	delete m_numbers;

	// Steal data
	m_size = rhs.m_size;
	m_numbers = rhs.m_numbers;

	// Clean up robbed object
	rhs.m_size = 0;
	rhs.m_numbers = nullptr;

	return *this;
}



FibonacciList::FibonacciList(FibonacciList&& rhs) noexcept
	: m_size(rhs.m_size),
	m_numbers(rhs.m_numbers) 
{
	std::cout << "MoveConstructor" << std::endl;
	rhs.m_numbers = nullptr;
}

int FibonacciList::getNumber(const size_t position) const
{
	if (position >= m_size) {
		throw std::invalid_argument("Position is out of range");
	}
	return m_numbers[position];
}

int* const FibonacciList::getNumbers() const noexcept
{
	return m_numbers;
}

size_t FibonacciList::getSize() const noexcept 
{
	return m_size;
}
