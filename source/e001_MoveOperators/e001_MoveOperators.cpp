// e001_MoveOperators.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "FibonacciList.h"

/*
    Further Reading:
    https://stackoverflow.com/q/3106110
*/

FibonacciList fibonacciFactory(size_t max_number)
{
    FibonacciList listSmall(max_number);
    FibonacciList listBig(max_number > 0 ? max_number : max_number-1);

    if (max_number < 5) {
        return listSmall;
    }
    else
        return listBig;
}

int main()
{    
    FibonacciList threeNumbers(3);

    // Copy the object
    FibonacciList copyOfThreeNumbers(threeNumbers);

    // Move constructor
    FibonacciList fiveNumbers(fibonacciFactory(5));

    // Move assignment
    fiveNumbers = std::move(threeNumbers);

    system("pause");
}