#pragma once
class FibonacciList
{
public:
	/// Allocates memory to store the first 'size' fibonacci numbers
	FibonacciList(size_t size);
	~FibonacciList();

	/// Copy constructor
	FibonacciList(const FibonacciList&) noexcept;

	/// Move constructor
	FibonacciList(FibonacciList&&) noexcept;

	/// Copy assignment 
	FibonacciList& operator=(const FibonacciList& rhs) noexcept;

	/// Move assignment
	FibonacciList& operator=(FibonacciList&& rhs) noexcept;



	// Not implemented functions
	/// Default constructor
	FibonacciList() = delete;





	/// Access single number,
	/// \return If position less than size, returns the corresponding fibonacci number.
	/// Otherwise throws invalid_argument
	int getNumber(const size_t position) const;

	/// Returns a pointer to #m_numbers
	int* const getNumbers() const noexcept;

	/// Returns #m_size
	size_t getSize() const noexcept;
private:
	size_t m_size;
	int* m_numbers;

};