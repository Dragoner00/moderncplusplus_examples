// e002_TypeTraits.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

// https://www.heise.de/developer/artikel/Typen-Nichttypen-und-Templates-als-Template-Parameter-4324511.html

#include <iostream>
#include <vector>
#include <type_traits>

//https://stackoverflow.com/q/27687389
template<typename ...>
using void_t = void;


template<typename Container, typename = void> // The second template parameter has no name, we just need it to get SFINAE working
struct hasSubscriptOperator 
    : std::false_type {}; // By inheritance, hasSubscriptOperator has a member 'value' which is 'false'

// https://en.cppreference.com/w/cpp/utility/declval
// Converts any type T to a reference type, making it possible to use member functions in decltype
// expressions without the need to go through constructors. 
template<typename Container>
struct hasSubscriptOperator<Container, void_t<decltype(std::declval<Container>()[0])>>
    : std::true_type {}; // By inheritance, hasSubscriptOperator has a member 'value' which is 'true'

template<typename Container>
using hasSubscriptOperator_t = typename hasSubscriptOperator<Container>::type;


// How std::enable_if works:
// template< bool B, class T = void >
// struct enable_if;
// If B is true, std::enable_if has a public member typedef type equal to T; otherwise, there is no member typedef
// Helpful: using enable_if_t = typename enable_if<B,T>::type;

template<typename Container, std::enable_if_t<hasSubscriptOperator_t<Container>::value>* = nullptr>
decltype(auto) accessContainer(Container&& container, size_t position) {
    std::cout << "Using subscript operator!" << std::endl;
    return std::forward<Container>(container)[position];
}


// Implement 'accessContainer' function which just works for call operators
template<typename Container, typename = void>
struct hasCallOperator
    : std::false_type {};

template<typename Container>
struct hasCallOperator<Container, void_t<decltype(std::declval<Container>()(0))>>
    : std::true_type {};

template<typename Container>
using hasCallOperator_t = typename hasCallOperator<Container>::type;

template<typename Container, std::enable_if_t<hasCallOperator_t<Container>::value>* = nullptr>
decltype(auto) accessContainer(Container&& container, size_t position) {
    std::cout << "Using call operator!" << std::endl;
    return std::forward<Container>(container)(position);
}


// For demonstration we need a container whose elements can only be accessed by the call-operator
class VectorWithCall {
private:
    std::vector<double> m_data;
public:
    VectorWithCall()
        : m_data(10, 3.21){
    }
    double& operator()(size_t index) {
        return m_data[index];
    }
};



int main()
{   
    std::vector<double> vectorWithSubcript(10, 1.23);
    VectorWithCall vectorWithCall;

    std::cout << accessContainer(vectorWithSubcript, 0) << std::endl;
    std::cout << accessContainer(vectorWithCall, 0) << std::endl;

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
